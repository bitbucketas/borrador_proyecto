const fs = require('fs');

const fileName = "./usersPassword.json";

function writeUserDataToFile(data) {
  var jsonUserData = JSON.stringify(data);

  fs.writeFile(fileName, jsonUserData, "utf8",
    function(err) {
      if(err) {
        console.log(err);
      } else {
        console.log("usuario persistido");
      }
    }
  );
}

module.exports.writeUserDataToFile = writeUserDataToFile;
