
const fileName = "../usersPassword.json";
const io = require('../io');

const requestJson = require("request-json");
const mLabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edasg/collections/";
const mLabAPIKey = "apiKey=rR72IfFJsD7JkQPBj4Trxn9YwL0VMp8Q";

function getUsersV1 (req,res){
  console.log("GET /apitechu/v1/users");
  // res.sendFile('usuarios.json', {root: __dirname});

  var users = require(fileName);

  var result = {};

  if(req.param("$count") != null && req.param("$count").toLowerCase() == "true" ){
    if (req.param("$top") && !isNaN(req.param("$top"))){
      result.users =  users.slice(0, req.param("$top"));
    }
    result.count = users.length;
  } else {
    result.users = (req.param("$top")&& !isNaN(req.param("$top"))) ? users.slice(0, req.param("$top")) : users;
  }

  res.send(result);
}

function getUsersV2 (req,res){
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(mLabBaseURL);
  console.log("Client created");

  httpClient.get("user?" + mLabAPIKey,
    function(err, resMLab, body) {
      var response = !err ?
        body : {"msg" : "Error obteniendo usuarios"}
        res.send(response);
    }
  );
}

function getUsersByIdV2 (req,res){
  console.log("GET /apitechu/v2/users/:id");

  var httpClient = requestJson.createClient(mLabBaseURL);
  console.log("Client created");

  var querie = 'q={"id":' + req.params.id  + '}';

  console.log('user?q={"id": '+req.params.id+"}&" + mLabAPIKey);


  httpClient.get('user?' + querie +"&" + mLabAPIKey,
    function(err, resMLab, body) {
      // var response = !err ?
      //   body : {"msg" : "Error obteniendo usuarios"}
      //   res.send(response);
        if(err) {
          var response = {"msg" : "Error obteniendo usuarios"};
          res.status(500);
        } else {
          if (body.lenght > 0) {
            var response = body[0];
          } else {
            var response = {"msg" : "Usuario no encontrado"};
            res.status(404);
          }
        }
        res.send(response);
    }
  );
}

function createUserV1(req,res){
  console.log("POST /apitechu/v1/users");

  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email
  };

  var users = require(fileName);
  users.push(newUser);

  io.writeUserDataToFile(users);

  res.send("Usuario añadido con éxito");
}

function createUserV2(req,res){
  console.log("POST /apitechu/v2/users");

  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.password);

  var newUser = {
    "id" : req.body.id,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : req.body.password
  };

  var httpClient = requestJson.createClient(mLabBaseURL);
  console.log("Client created");

  httpClient.post("user?" + mLabAPIKey, newUser,
    function(err, resMLab, body) {
      console.log("Usuario guardado con exito");
      res.status(201);
      res.send('{"msg" : "Usuario guardado con exito"}');
    }
  );
}

function deleteUserV1(req,res){
  console.log("DELETE /apitechu/v1/users/:id");

  var users = require(fileName);

  // for (var i = 0; i < users.length; i++) {
  //   if(users[i].id == req.params.id){
  //     console.log(users[i]);
  //     users.splice(i, 1);
  //
  //     break;
  //   }
  // }

  // for (i in users) {
  //     if(users[i].id == req.params.id){
  //       console.log(users[i]);
  //       users.splice(i, 1);
  //
  //       break;
  //     }
  // }

  // for (user of users) {
  //   if(user.id == req.params.id){
  //     var i = users.indexOf(user);
  //     console.log(users[i]);
  //     users.splice(i, 1);
  //
  //     break;
  //   }
  // }

  // users.forEach(function(user, index, array) {
  //
  //   if (user.id == req.params.id) {
  //     console.log(index);
  //     users.splice(index, 1);
  //   }
  // });


  var user = users.find(function(element) {
    return element.id ==  req.params.id;
  });
  users.splice(users.indexOf(user), 1);

  io.writeUserDataToFile(users);

  res.send("stop waiting..." + req.params.id);
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUsersByIdV2 = getUsersByIdV2;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
