const fileName = "../usersPassword.json";
const io = require('../io');

function loginV1(req,res){
  console.log("POST /apitechu/v1/login");

  var body = req.body;

  if (body && body.email && body.password){

    var users = require(fileName);

    var indexOfElement = users.findIndex(
      function(element){
        return (element.email == body.email) && (element.password == body.password);
      }
    )

    if(indexOfElement != -1) {
      users[indexOfElement].logged = true;
      io.writeUserDataToFile(users);
      res.send('{ "mensaje" : "Login correcto", "idUsuario" : ' + users[indexOfElement].id + ' }');
    } else {
      res.send('{ "mensaje" : "Login incorrecto" }');
    }

  } else {
    res.send('{ "mensaje" : "Error: los datos del body son incorrectos"}');
  }
}

function logoutV1(req,res){
  console.log("POST /apitechu/v1/logout");

  var body = req.body;

  if (body && body.id){

    var users = require(fileName);

    var indexOfElement = users.findIndex(
      function(element){
        return element.id == body.id;
      }
    )

    if(indexOfElement != -1 && users[indexOfElement].logged) {
      delete users[indexOfElement].logged;
      io.writeUserDataToFile(users);
      res.send('{ "mensaje" : "Logout correcto", "idUsuario" : ' + users[indexOfElement].id + ' }');
    } else {
      res.send('{ "mensaje" : "Logout incorrecto" }');
    }

  } else {
    res.send('{ "mensaje" : "Error: los datos del body son incorrectos"}');
  }
}


module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
