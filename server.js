const express = require('express');
const app = express();
app.use(express.json());

const requestJson = require("request-json");
const mLabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edasg/collections/";
const mLabAPIKey = "apiKey=rR72IfFJsD7JkQPBj4Trxn9YwL0VMp8Q";

const io = require('./io');
const userController = require('./controllers/userController');
const authController = require('./controllers/authController');



const port = process.env.PORT || 3000;
app.listen(port);
console.log("Server iniciado en el puerto " + port);



app.get("/apitechu/v1/users", userController.getUsersV1);
app.get("/apitechu/v2/users", userController.getUsersV2);
app.get("/apitechu/v2/users/:id", userController.getUsersByIdV2);

app.post("/apitechu/v1/users", userController.createUserV1);
app.post("/apitechu/v2/users", userController.createUserV2);

app.delete("/apitechu/v1/users/:id", userController.deleteUserV1);

app.post("/apitechu/v1/login", authController.loginV1);

app.post("/apitechu/v1/logout", authController.logoutV1);


app.get("/apitechu/v1/hello",
  function(req,res){
    console.log("GET /apitechu/v1/hello");
    res.send({"msg" : "Bienvenido al auto-mágico mundo del API TechU!"});
  }
)

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function (req,res){
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");

    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);

    res.send("stop waiting...");
  }
)
